class JobsController < ApplicationController

  # before_action :set_params, only: []

  def index
    @newsletter = Newsletter.new

    respond_to do |format|      
      if params[:search]
      
        @jobs = Job.page.search(params[:search]).per(10)
        format.html
        format.js {}
      else
        @jobs = Job.page(params[:page]).per(5).order("created_at DESC")
        format.html
        format.js          
      end  
    end
  end

  def new
  	@job = Job.new
  end

  def create
    @job = Job.new(job_params)
    if @job.save
      flash[:notice] = "Vaga Anunciada com Sucesso!"
      redirect_to @job
    else
      flash[:alert] = "Erro ao cadastrar a vaga!"
      render :new
    end
  end

  def show
    @job = Job.friendly.find(params[:id])
  end

  def about
  end

  private

  def job_params
  	params.require(:job).permit(:title, :company, :url, :description, :slug, :email, :telephone)
  end
end
