class NewsletterController < ApplicationController

  def create

  	@newsletter = Newsletter.new(newsletrer_params)
  	if @newsletter.save!     
  	  flash[:notice] = "Obrigado por se cadastrar em nossa newsletter!"  	  
      redirect_to root_path
  	else
  		flash[:alert] = "Erro ao cadastrar email em nossa newsletter."
      redirect_to root_path
  	end
  end

  private 

  def newsletrer_params
    params.require(:newsletter).permit(:email)
  end
end