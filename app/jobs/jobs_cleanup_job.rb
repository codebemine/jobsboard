require 'date'

class JobsCleanupJob < ActiveJob::Base
  queue_as :default

  def perform(jobs)
    jobs.each do |job|
      job.destroy if job.created_at <= DateTime.current.weeks_ago(12)
    end
  end
end