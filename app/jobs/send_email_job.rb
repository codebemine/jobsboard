class SendEmailJob < ActiveJob::Base
  queue_as :default

  def perform(emails, jobs)
    emails.each do |email|
      SenderNewsletter.send_newsletter(email, jobs).deliver_later
    end
  end
end
