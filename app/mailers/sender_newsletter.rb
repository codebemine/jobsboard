class SenderNewsletter < ApplicationMailer
	def send_newsletter(email, jobs)
		@email  = email
		@jobs = jobs
		mail(to: @email, subject: "Vagas recentes")
	end
end
