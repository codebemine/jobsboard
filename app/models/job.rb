class Job < ActiveRecord::Base

	validates :title, presence: true
  validates :company, presence: true
  validates :description, presence: true
  # validates :email, format: { with: /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/ }
  # include PgSearch
  extend FriendlyId

  # pg_search_scope :search_everywhere, :against => [:title, :company], :ignoring => :accents

  
  friendly_id :slug_candidates, use: :slugged

  def slug_candidates
    [:title, :created_at]
  end

  def self.garbage_collector
    JobsCleanupJob.perform_later(Job.first(50))
  end

  #Implementar Full Text Search na versão 2.0
  def self.search(query)
    where("lower(title) like ?", "%#{query.downcase}%") 
  end

  private

  def upper
    "#{self.title.upcase}"
  end

end