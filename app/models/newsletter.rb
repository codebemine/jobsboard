class Newsletter < ActiveRecord::Base
	validates :email, presence: true#, format: { with: /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/ }

	def self.sender
		@jobs = Job.last(5)
		@emails = Newsletter.all.pluck(:email)
		SendEmailJob.perform_later(@emails, @jobs)
	end
end