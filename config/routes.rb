Rails.application.routes.draw do

  get "vaga/nova" => "jobs#new", as: "new_job"
  post "vaga/nova" => "jobs#create", as: nil

  get "vaga/:id" => "jobs#show", as: "job"

  get "sobre" => "jobs#about", as: "about_job"

  root "jobs#index"

  post "jobs" => "newsletter#create"

  get "jobs" => "jobs#index", path: "vagas"

  # resources :jobs
  # resources :newsletters

end