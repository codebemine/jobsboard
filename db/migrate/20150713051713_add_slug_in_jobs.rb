class AddSlugInJobs < ActiveRecord::Migration
  def change
  	add_column :jobs, :slug, :string, uniqueness: true
  end
end
