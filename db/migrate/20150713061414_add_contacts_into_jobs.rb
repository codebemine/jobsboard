class AddContactsIntoJobs < ActiveRecord::Migration
  def change
  	add_column :jobs, :email, :string
  	add_column :jobs, :telephone, :string
  end
end
