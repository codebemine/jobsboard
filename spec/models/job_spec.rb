require 'rails_helper'

RSpec.describe Job, type: :model do

	before(:all) do
    job = Job.new!(title: "job_title", company: "job_company", description: "job_description")
	end
  
  it "title with slug candidates" do
  	expect(job.slug_candidates).to eq([job.title, job.created_at])
  end

  it "title to be uppercase" do
  	upcase = job.upper(job.title)

  	expect(upcase).to eq("JOB_TITLE")
  end

end
